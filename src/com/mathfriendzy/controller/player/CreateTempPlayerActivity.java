package com.mathfriendzy.controller.player;

import static com.mathfriendzy.utils.ICommonUtils.CREATE_TEMP_PLAYER_FLAG;
import static com.mathfriendzy.utils.ICommonUtils.IS_LOGIN;
import static com.mathfriendzy.utils.ICommonUtils.LOGIN_SHARED_PREFF;
import com.mathfriendzy.controller.base.AdBaseActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.languagefriendzypaid.R;
import com.mathfriendzy.controller.login.LoginActivity;
import com.mathfriendzy.controller.main.MainActivity;
import com.mathfriendzy.controller.top100.Top100Activity;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.utils.DialogGenerator;

/**
 * This activity is open when user click on player button and player is not exist then
 * this activity is open 
 * @author Yashwant Singh
 *
 */
public class CreateTempPlayerActivity extends AdBaseActivity implements OnClickListener
{
	private TextView mfTitleHomeScreen = null;
	private Button   btnTitleTop100    = null;
	private TextView lblRegisteredUser = null;
	private Button   alertBtnLogin     = null;
	private TextView lblNewUsers       = null;
	private Button   lblCreatePlayer   = null;
	private Button   btnTitleBack      = null;
	
	private String TAG = this.getClass().getSimpleName();
	
	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_create_temp_player);
		
		if(CREATE_TEMP_PLAYER_FLAG)
			Log.e(TAG, "inside onClick()");
		
		this.setWidgetsReferences();
		this.setListenerOnwidgets();
		this.setWidgetsTextValue();
		
		if(CREATE_TEMP_PLAYER_FLAG)
			Log.e(TAG, "outside onClick()");
	}

	/**
	 * This method set tje widgets references from the layout to widgets objects
	 */
	private void setWidgetsReferences()
	{
		if(CREATE_TEMP_PLAYER_FLAG)
			Log.e(TAG, "inside setWidgetsReferences()");
		
		lblCreatePlayer 		= (Button)   findViewById(R.id.lblCreatePlayer);
		mfTitleHomeScreen 		= (TextView) findViewById(R.id.mfTitleHomeScreen);
		btnTitleTop100          = (Button)   findViewById(R.id.btnTitleTop100);
		lblRegisteredUser       = (TextView) findViewById(R.id.lblRegisteredUser);
		alertBtnLogin           = (Button)   findViewById(R.id.alertBtnLogin);
		lblNewUsers				= (TextView) findViewById(R.id.lblNewUsers);
		btnTitleBack            = (Button)   findViewById(R.id.btnTitleBack);
		
		if(CREATE_TEMP_PLAYER_FLAG)
			Log.e(TAG, "outside setWidgetsReferences()");
	}
	
	/**
	 * This method set the listener on widgets
	 */
	private void setListenerOnwidgets()
	{
		if(CREATE_TEMP_PLAYER_FLAG)
			Log.e(TAG, "inside setListenerOnwidgets()");
		
		lblCreatePlayer.setOnClickListener(this);
		btnTitleBack.setOnClickListener(this);
		alertBtnLogin.setOnClickListener(this);
		btnTitleTop100.setOnClickListener(this);
				
		if(CREATE_TEMP_PLAYER_FLAG)
			Log.e(TAG, "outside setListenerOnwidgets()");
	}
	
	/**
	 * This method set the widgets text values from transelation
	 */
	private void setWidgetsTextValue()
	{
		if(CREATE_TEMP_PLAYER_FLAG)
			Log.e(TAG, "inside setWidgetsTextValue()");
		
		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		mfTitleHomeScreen.setText(transeletion.getTranselationTextByTextIdentifier("homeTitleFriendzy"));
		btnTitleTop100.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleTop100"));
		lblRegisteredUser.setText(transeletion.getTranselationTextByTextIdentifier("lblRegisteredUser") + ":");
		alertBtnLogin.setText(transeletion.getTranselationTextByTextIdentifier("alertBtnLogin"));
		lblCreatePlayer.setText(transeletion.getTranselationTextByTextIdentifier("lblCreatePlayer"));
		lblNewUsers.setText(transeletion.getTranselationTextByTextIdentifier("lblNewUsers") + ":");
		btnTitleBack.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleBack"));
		transeletion.closeConnection();
		
		if(CREATE_TEMP_PLAYER_FLAG)
			Log.e(TAG, "outside setWidgetsTextValue()");
	}

	@Override
	public void onClick(View v) 
	{
		switch(v.getId())
		{
		case R.id.lblCreatePlayer:
			Intent intent = new Intent(this,AddTempPlayerStep1Activity.class);
			startActivity(intent);
			break;
		case R.id.btnTitleBack:
			Intent intentMain = new Intent(this,MainActivity.class);
			startActivity(intentMain);
			break;
		case R.id.alertBtnLogin:
			Intent intentLogin = new Intent(this, LoginActivity.class);
			intentLogin.putExtra("callingActivity", "CreateTempPlayerActivity");
			startActivity(intentLogin);
			break;
		case R.id.btnTitleTop100 :
			SharedPreferences sheredPreference = getSharedPreferences(LOGIN_SHARED_PREFF, 0);
			if(!sheredPreference.getBoolean(IS_LOGIN, false))
			{
				Translation transeletion = new Translation(this);
				transeletion.openConnection();
				DialogGenerator dg = new DialogGenerator(this);
				dg.generateRegisterOrLogInDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouMustLoginOrRegisterToViewAndParticipate"));
				transeletion.closeConnection();	
			}
			else
			{
			   startActivity(new Intent(this,Top100Activity.class));
			}
			break;
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) 
	{
		if(keyCode == KeyEvent.KEYCODE_BACK)
		{
			Intent intentMain = new Intent(this,MainActivity.class);
			startActivity(intentMain);
			return false;
		}
		return super.onKeyDown(keyCode, event);
	}
}
