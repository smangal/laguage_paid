package com.mathfriendzy.controller.player;

import static com.mathfriendzy.utils.ICommonUtils.ADD_OR_CREATE_USER_PLAYER_FLAG;
import static com.mathfriendzy.utils.ICommonUtils.IS_CHECKED_PREFF;
import static com.mathfriendzy.utils.ICommonUtils.IS_FACEBOOK_LOGIN;
import static com.mathfriendzy.utils.ICommonUtils.IS_LOGIN_FROM_FACEBOOK;
import static com.mathfriendzy.utils.ICommonUtils.PLAYER_ID;

import java.util.ArrayList;

import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.mathfriendzy.controller.chooseavtar.ChooseAvtars;
import com.mathfriendzy.controller.chooseavtar.GridAdapter;
import com.mathfriendzy.controller.main.MainActivity;
import com.mathfriendzy.controller.registration.ModifyRegistration;
import com.mathfriendzy.controller.school.SearchTeacherActivity;
import com.mathfriendzy.controller.school.SearchYourSchoolActivity;
import com.mathfriendzy.facebookconnect.FacebookConnect;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.listener.EditTextFocusChangeListener;
import com.mathfriendzy.model.country.Country;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.registration.Register;
import com.mathfriendzy.model.registration.RegistereUserDto;
import com.mathfriendzy.model.registration.UserPlayerDto;
import com.mathfriendzy.model.registration.UserPlayerOperation;
import com.mathfriendzy.model.registration.UserRegistrationOperation;
import com.mathfriendzy.model.states.States;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.DialogGenerator;
import com.mathfriendzy.utils.Validation;
import com.languagefriendzypaid.R;

/**
 *@description This class Add or create new Player
 * @author Yashwant Singh
 *
 */
public class AddPlayer extends BasePlayer implements OnClickListener , EditTextFocusChangeListener
{
    private Button   btnSavePlayer  	= null;
    private TextView lblAddPlayerTitle  = null;
    private TextView lblTransferYourInfoFromFacebookAndFindFriends = null;

    public static String schoolName				  = null;
    private String country 						  = null;
    private RegistereUserDto regUserObj 		  = null;
    private final String IMAGE_NAME				  = "Smiley";

    public static boolean IS_REGISTER_SCHOOL	  = false;
    public static Bitmap profileImageBitmap       = null;
    private String schoolIdFromFind  			  = null;

    ProgressDialog progressDialog = null;

    private  ArrayList<String> teacherInfo 		= null;
    private String teacherFirstName 			= null;
    private String teacherLastName  			= null;
    private String teacherId        			= null;

    private SharedPreferences facebookPreference= null;

    private String callingActivity = null;
    private Button   btnTitleBack  = null;

    private final String TAG = this.getClass().getSimpleName();
    private boolean onFocusLostIsValidString = true;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_addor_create_user_player);

        if(ADD_OR_CREATE_USER_PLAYER_FLAG)
            Log.e(TAG, "inside oncreate()");

        facebookPreference = getSharedPreferences(IS_FACEBOOK_LOGIN, 0);
        imageName = IMAGE_NAME;

        callingActivity = this.getIntent().getStringExtra("callingActivity");
        this.init();
        this.setWidgetsReferences();
        this.setWidgetsTextValue();
        //this.getGrade(this,"");
        this.setGrade(MathFriendzyHelper.GRADE_TEXT);
        this.setListenerOnWidgets();

        if(ADD_OR_CREATE_USER_PLAYER_FLAG)
            Log.e(TAG, "outside oncreate()");
    }

    private void init(){
        gradeList = MathFriendzyHelper.getUpdatedGradeList(this);
    }

    private void setGrade(String grade){
        MathFriendzyHelper.setAdapterToSpinner(this, grade , gradeList, spinnerGrade);
    }

    /**
     * This method set the widgets references from layout to the widgets
     */
    private void setWidgetsReferences()
    {
        if(ADD_OR_CREATE_USER_PLAYER_FLAG)
            Log.e(TAG, "inside setWidgetsReferences()");

        edtFirstName 	= (EditText) findViewById(R.id.edtFirstName);
        edtLastName  	= (EditText) findViewById(R.id.edtLastName);
        edtUserName  	= (EditText) findViewById(R.id.edtUserName);

        spinnerGrade 	= (Spinner) findViewById(R.id.spinnerGrade);
        edtSchool 		= (EditText) findViewById(R.id.spinnerSchool);//changes
        edtTeacher      = (EditText) findViewById(R.id.spinnerTeacher);//changes
        btnSavePlayer   = (Button) findViewById(R.id.btnTitleSave);
        imgAvtar        = (ImageView) findViewById(R.id.imgAvtar);

        mfTitleHomeScreen 	= (TextView) findViewById(R.id.mfTitleHomeScreen);
        lblAddPlayerTitle 	= (TextView) findViewById(R.id.lblAddPlayerTitle);
        lblFbConnect 		= (Button) findViewById(R.id.lblFbConnect);
        lblFirstName 		=  (TextView) findViewById(R.id.lblFirstName);
        lblLastName 		= (TextView) findViewById(R.id.lblLastName);
        lblUserName 		= (TextView) findViewById(R.id.lblUserName);
        pickerTitleSchool   = (TextView) findViewById(R.id.pickerTitleSchool);
        lblAddPlayerGrade 	= (TextView) findViewById(R.id.lblAddPlayerGrade);
        lblRegTeacher 		= (TextView) findViewById(R.id.lblRegTeacher);
        lblChooseAnAvatar 	= (TextView) findViewById(R.id.lblChooseAnAvatar);
        avtarLayout         = (RelativeLayout) findViewById(R.id.avtarLayout);

        lblTransferYourInfoFromFacebookAndFindFriends = (TextView) findViewById(R.id.lblTransferYourInfoFromFacebookAndFindFriends);

        btnTitleBack        = (Button)   findViewById(R.id.btnTitleBack);

        //set Done button to keyboard
        MathFriendzyHelper.setDoneButtonToEditText(edtFirstName);
        MathFriendzyHelper.setDoneButtonToEditText(edtLastName);
        MathFriendzyHelper.setDoneButtonToEditText(edtUserName);
        //end changes

        if(ADD_OR_CREATE_USER_PLAYER_FLAG)
            Log.e(TAG, "outside setWidgetsReferences()");

    }

    protected void setHintToEditText(String text ,  EditText edtText){
        edtText.setHint(text);
    }

    /**
     * This method set the text values from the transelation
     */
    private void setWidgetsTextValue()
    {
        if(ADD_OR_CREATE_USER_PLAYER_FLAG)
            Log.e(TAG, "inside setWidgetsTextValue()");

        Translation transeletion = new Translation(this);
        transeletion.openConnection();
        mfTitleHomeScreen.setText(MathFriendzyHelper.getAppName(transeletion));
        lblAddPlayerTitle.setText(transeletion.getTranselationTextByTextIdentifier("lblAddPlayerTitle") + " :");
        lblFirstName.setText(transeletion.getTranselationTextByTextIdentifier("lblFirstName") + " :");
        lblLastName.setText(transeletion.getTranselationTextByTextIdentifier("lblLastInitial") + " :");
        lblUserName.setText(transeletion.getTranselationTextByTextIdentifier("lblUserName") + " :");
        pickerTitleSchool.setText(transeletion.getTranselationTextByTextIdentifier("pickerTitleSchool") + " :");
        lblAddPlayerGrade.setText(transeletion.getTranselationTextByTextIdentifier("lblAddPlayerGrade") + " :");
        lblRegTeacher.setText(transeletion.getTranselationTextByTextIdentifier("lblRegTeacher") + " :");
        lblChooseAnAvatar.setText("Avatar");
        btnSavePlayer.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleSubmit"));
        this.setHintToEditText(transeletion.getTranselationTextByTextIdentifier("lblFirstName"), edtFirstName);
        this.setHintToEditText(transeletion.getTranselationTextByTextIdentifier("lblLastInitial"), edtLastName);
        this.setHintToEditText(transeletion.getTranselationTextByTextIdentifier("lblUserName"), edtUserName);

        if(!facebookPreference.getBoolean(IS_LOGIN_FROM_FACEBOOK, false))
        {
            lblFbConnect.setText(transeletion.getTranselationTextByTextIdentifier("lblFbConnect"));
            lblTransferYourInfoFromFacebookAndFindFriends.setText(transeletion.getTranselationTextByTextIdentifier
                    ("lblTransferYourInfoFromFacebookAndFindFriends"));
        }
        else
        {
            lblFbConnect.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleLogOut"));
            lblTransferYourInfoFromFacebookAndFindFriends.setText(transeletion.getTranselationTextByTextIdentifier
                    ("lblDisconnectFromFb"));
        }

        btnTitleBack.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleBack"));
        transeletion.closeConnection();

        if(ADD_OR_CREATE_USER_PLAYER_FLAG)
            Log.e(TAG, "outside setWidgetsTextValue()");

    }

    /**
     * This method get Country name from database
     * @return
     */
    private String getCountryName()
    {
        if(ADD_OR_CREATE_USER_PLAYER_FLAG)
            Log.e(TAG, "inside getCountryName()");

        UserRegistrationOperation regObj = new UserRegistrationOperation(AddPlayer.this);
        regUserObj = regObj.getUserData();
        Country countryObj = new Country();
        country = countryObj.getCountryNameByCountryId(regUserObj.getCountryId(), AddPlayer.this);

        if(ADD_OR_CREATE_USER_PLAYER_FLAG)
            Log.e(TAG, "outside getCountryName()");

        return country;
    }

    /**
     * Set listener on widgets
     */
    private void setListenerOnWidgets()
    {
        if(ADD_OR_CREATE_USER_PLAYER_FLAG)
            Log.e(TAG, "inside setListenerOnWidgets()");

        btnSavePlayer.setOnClickListener(this);
        lblFbConnect.setOnClickListener(this);
        avtarLayout.setOnClickListener(this);

        edtSchool.setOnClickListener(this);//changes
        edtTeacher.setOnClickListener(this);//changes

        btnTitleBack.setOnClickListener(this);

        //changes according deepak mail
        spinnerGrade.setOnItemSelectedListener(new OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> arg0, View arg1,
                                       int arg2, long arg3) {

                int selectedGrade = MathFriendzyHelper.getGrade
                        (spinnerGrade.getSelectedItem().toString());

                if((selectedGrade + "" ).equals("13"))
                {
                    edtSchool.setVisibility(EditText.GONE);
                    edtTeacher.setVisibility(EditText.GONE);
                    //lblRegTeacher.setVisibility(TextView.GONE);
                    //pickerTitleSchool.setVisibility(TextView.GONE);
                }else{
                    edtSchool.setVisibility(EditText.GONE);
                    edtTeacher.setVisibility(EditText.GONE);
                    //lblRegTeacher.setVisibility(TextView.VISIBLE);
                    //pickerTitleSchool.setVisibility(TextView.VISIBLE);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
                // TODO Auto-generated method stub

            }
        });
        //end changes

        MathFriendzyHelper.setFocusChangeListener(this , edtFirstName , this);
        MathFriendzyHelper.setFocusChangeListener(this , edtLastName , this);
        MathFriendzyHelper.setFocusChangeListener(this , edtUserName , this);

        MathFriendzyHelper.setEditTextWatcherToEditTextForLastInitialName(this , edtLastName);

        if(ADD_OR_CREATE_USER_PLAYER_FLAG)
            Log.e(TAG, "outside setListenerOnWidgets()");

    }

    @Override
    public void onClick(View v)
    {
        MathFriendzyHelper.clearFocus(this);
        if(onFocusLostIsValidString) {
            switch (v.getId()) {
                case R.id.avtarLayout:
                    GridAdapter.isAddNewPlayer = true;
                    Intent chooseAvtar = new Intent(this, ChooseAvtars.class);
                    startActivity(chooseAvtar);
                    break;
                case R.id.btnTitleSave:
                    this.checkEmptyValidate();
                    break;
                case R.id.lblFbConnect:
                    this.facebookConnect();
                    break;
                case R.id.spinnerSchool:
                    this.setListenerOnEdtSchool();
                    break;
                case R.id.spinnerTeacher:
                    Intent teacherIntent = new Intent(this, SearchTeacherActivity.class);
                    teacherIntent.putExtra("schoolId", schoolIdFromFind);
                    startActivityForResult(teacherIntent, 2);
                    break;
                case R.id.btnTitleBack:
                    if (callingActivity.equals("ModifyRegistration"))
                        startActivity(new Intent(AddPlayer.this, ModifyRegistration.class));
                    else if (callingActivity.equals("LoginUserCreatePlayer"))
                        startActivity(new Intent(AddPlayer.this, LoginUserPlayerActivity.class));
                    else if (callingActivity.equals("LoginUserPlayerActivity"))
                        startActivity(new Intent(AddPlayer.this, LoginUserPlayerActivity.class));
                    else if (callingActivity.equals("CreateTeacherPlayerActivity"))
                        startActivity(new Intent(AddPlayer.this, CreateTeacherPlayerActivity.class));
                    else if (callingActivity.equals("TeacherPlayer"))
                        startActivity(new Intent(AddPlayer.this, TeacherPlayer.class));
                    break;
            }
        }
    }

    /**
     * This method connect with facebook and if already connect to facebook then disconnect from faebook
     */
    private void facebookConnect()
    {
        Translation transeletion1 = new Translation(this);
        transeletion1.openConnection();

        SharedPreferences.Editor editor = facebookPreference.edit();

        if(facebookPreference.getBoolean(IS_LOGIN_FROM_FACEBOOK, false))
        {
            FacebookConnect.logoutFromFacebook();
            editor.putBoolean(IS_LOGIN_FROM_FACEBOOK, false);
            editor.commit();
            lblFbConnect.setText(transeletion1.getTranselationTextByTextIdentifier("lblFbConnect"));
            lblTransferYourInfoFromFacebookAndFindFriends.setText(transeletion1.getTranselationTextByTextIdentifier
                    ("lblTransferYourInfoFromFacebookAndFindFriends"));
        }
        else
        {
            //changes for Internet Connection
            if(CommonUtils.isInternetConnectionAvailable(this))
            {
                Intent intent = new Intent(this,FacebookConnect.class);
                startActivity(intent);
                editor.putBoolean(IS_LOGIN_FROM_FACEBOOK, true);
                editor.commit();
                lblFbConnect.setText(transeletion1.getTranselationTextByTextIdentifier("btnTitleLogOut"));
                lblTransferYourInfoFromFacebookAndFindFriends.setText(transeletion1.getTranselationTextByTextIdentifier
                        ("lblDisconnectFromFb"));
            }
            else
            {
                DialogGenerator dg1 = new DialogGenerator(this);
                dg1.generateWarningDialog(transeletion1.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
            }
        }
        transeletion1.closeConnection();
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if(keyCode == KeyEvent.KEYCODE_BACK)
        {
            if(callingActivity.equals("ModifyRegistration"))
                startActivity(new Intent(AddPlayer.this,ModifyRegistration.class));
            else if(callingActivity.equals("LoginUserCreatePlayer"))
                startActivity(new Intent(AddPlayer.this,LoginUserCreatePlayer.class));
            else if(callingActivity.equals("LoginUserPlayerActivity"))
                startActivity(new Intent(AddPlayer.this,LoginUserPlayerActivity.class));
            else if(callingActivity.equals("CreateTeacherPlayerActivity"))
                startActivity(new Intent(AddPlayer.this,CreateTeacherPlayerActivity.class));
            else if(callingActivity.equals("TeacherPlayer"))
                startActivity(new Intent(AddPlayer.this,TeacherPlayer.class));
            return false;

        }
        return super.onKeyDown(keyCode, event);
    }


    /**
     * This method is called when action perform on School edit text
     */
    private void setListenerOnEdtSchool()
    {
        if(ADD_OR_CREATE_USER_PLAYER_FLAG)
            Log.e(TAG, "inside setListenerOnEdtSchool()");

        String country = getCountryName();

        Intent schoolIntent = new Intent(this,SearchYourSchoolActivity.class);

        //for getting state name
        States stateObj  = new States();
        String stateName = null;
        if(country.equals("United States"))
        {
            stateName = stateObj.getUSStateNameById(regUserObj.getStateId(), this);
        }
        else if(country.equals("Canada"))
        {
            stateName = stateObj.getCanadaStateNameById(regUserObj.getStateId(), this);
        }


        if(country.equals("United States") ||
                country.equals("Canada"))
        {
            schoolIntent.putExtra("country", country);
            schoolIntent.putExtra("state", stateName);
            schoolIntent.putExtra("city", regUserObj.getCity());
            schoolIntent.putExtra("zip", regUserObj.getZip());
        }
        else
        {
            schoolIntent.putExtra("country", country);
            schoolIntent.putExtra("state", "");
            schoolIntent.putExtra("city", regUserObj.getCity());
            schoolIntent.putExtra("zip", regUserObj.getZip());
        }
        startActivityForResult(schoolIntent, 1);


        if(ADD_OR_CREATE_USER_PLAYER_FLAG)
            Log.e(TAG, "outside setListenerOnEdtSchool()");
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if(ADD_OR_CREATE_USER_PLAYER_FLAG)
            Log.e(TAG, "inside onActivityResult()");

		/*if (requestCode == 1) 
		{
		     if(resultCode == RESULT_OK)
		     {      
		         ArrayList<String> schoolInfo = data.getStringArrayListExtra("schoolInfo");
		         edtSchool.setText(schoolInfo.get(0));
		         schoolIdFromFind = schoolInfo.get(1);
		     }

		 }*/

        if (requestCode == 1)
        {
            if(resultCode == RESULT_OK)
            {
                ArrayList<String> schoolInfo = data.getStringArrayListExtra("schoolInfo");
                edtSchool.setText(schoolInfo.get(0));
                schoolIdFromFind = schoolInfo.get(1);

                if(schoolInfo.get(0).equals("Home School"))
                {
                    edtTeacher.setEnabled(false);
                    edtTeacher.setText("N/A");
                    teacherFirstName = "N/A";
                    teacherLastName  = "";
                    teacherId        = "-2";
                }
                else
                {
                    edtTeacher.setEnabled(true);
                }
            }
        }

        if (requestCode == 2)
        {
            if(resultCode == RESULT_OK)
            {
                teacherInfo = data.getStringArrayListExtra("schoolInfo");
                edtTeacher.setText(teacherInfo.get(0) + " " + teacherInfo.get(1));

                teacherFirstName = teacherInfo.get(0);
                teacherLastName  = teacherInfo.get(1);
                teacherId        = teacherInfo.get(2);
            }

        }

        if(ADD_OR_CREATE_USER_PLAYER_FLAG)
            Log.e(TAG, "outside onActivityResult()");

        super.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * This method check empty validation
     */
    private void checkEmptyValidate()
    {
        if(ADD_OR_CREATE_USER_PLAYER_FLAG)
            Log.e(TAG, "inside checkEmptyValidate()");

        Translation transeletion = new Translation(this);
        transeletion.openConnection();
        DialogGenerator dg = new DialogGenerator(this);

        int grade = MathFriendzyHelper.getGrade(spinnerGrade.getSelectedItem().toString());
        if(edtFirstName.getText().toString().equals("")
                ||edtLastName.getText().toString().equals("")
                ||edtUserName.getText().toString().equals("")//condition change according deepak mail
                || (grade == MathFriendzyHelper.NO_GRADE_SELETED)
				/*||( (!(grade + "").equals("13")) &&
				( edtSchool.getText().toString().equals("") 
						|| edtTeacher.getText().toString().equals("")))*/
                )
        {
            dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgPleaseEnterAllInfo"));
        }
        else
        {
            this.checkValidUser(edtUserName.getText().toString());
        }
        transeletion.closeConnection();

        if(ADD_OR_CREATE_USER_PLAYER_FLAG)
            Log.e(TAG, "outside checkEmptyValidate()");

    }

    /**
     * Check for user is valid or not
     * @param userName
     */
    private void checkValidUser(String userName)
    {
        if(ADD_OR_CREATE_USER_PLAYER_FLAG)
            Log.e(TAG, "inside checkValidUser()");

        if(CommonUtils.isInternetConnectionAvailable(this))
        {
            progressDialog = CommonUtils.getProgressDialog(this);
            new CkeckValidUserAsyncTask(userName).execute(null,null,null);
        }
        else
        {
            DialogGenerator dg = new DialogGenerator(this);
            Translation transeletion = new Translation(this);
            transeletion.openConnection();
            dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
            transeletion.closeConnection();
        }

        if(ADD_OR_CREATE_USER_PLAYER_FLAG)
            Log.e(TAG, "outside checkValidUser()");
    }

    /**
     * This method save the temp  player data to the database
     */
    public void savePlayerData()
    {
        if(ADD_OR_CREATE_USER_PLAYER_FLAG)
            Log.e(TAG, "inside savePlayerData()");

        UserRegistrationOperation userData = new UserRegistrationOperation(this);
        RegistereUserDto userDataObj = userData.getUserData();

        UserPlayerOperation userObj1 = new UserPlayerOperation(this);
        ArrayList<UserPlayerDto> userPlayerObj = userObj1.getUserPlayerData();

        UserPlayerDto userPlayer = new UserPlayerDto();
        userPlayer.setPlayerid("0");
        userPlayer.setFirstname(edtFirstName.getText().toString());
        userPlayer.setLastname(edtLastName.getText().toString());

        int selectedGrade = MathFriendzyHelper
                .getGrade(spinnerGrade.getSelectedItem().toString());
        userPlayer.setGrade(selectedGrade + "");

        //for always hide the school and teacher
        userPlayer.setSchoolId("0");
        userPlayer.setTeacherUserId("0");

        //changes according to deepak mail
		/*if(spinnerGrade.getSelectedItem().toString().equals("13")){
			userPlayer.setSchoolId("0");
			userPlayer.setTeacherUserId("0");
		}
		else{
			userPlayer.setSchoolId(schoolIdFromFind);
			userPlayer.setTeacherUserId(teacherId);
		}*/
        //end changes

        userPlayer.setIndexOfAppearance("-1");
        userPlayer.setImageName(imageName);
        userPlayer.setCoin("0");
        userPlayer.setPoints("0");

        userPlayer.setUsername(edtUserName.getText().toString());
        userPlayer.setCompletelavel("1");

        userPlayerObj.add(userPlayer);

        userDataObj.setPlayers("<AllPlayers>"+this.getXmlPlayer(userPlayerObj)+"</AllPlayers>");

        if(CommonUtils.isInternetConnectionAvailable(this))
        {
            new UpdateAsynTask(userDataObj).execute(null,null,null);
        }
        else
        {
            DialogGenerator dg = new DialogGenerator(this);
            Translation transeletion = new Translation(this);
            transeletion.openConnection();
            dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
            transeletion.closeConnection();
        }

        if(ADD_OR_CREATE_USER_PLAYER_FLAG)
            Log.e(TAG, "outside savePlayerData()");
    }

    /**
     * This method return the xml formate of the user player
     * @param userPlayerObj
     * @return
     */
    private String getXmlPlayer(ArrayList<UserPlayerDto> userPlayerObj)
    {
        if(ADD_OR_CREATE_USER_PLAYER_FLAG)
            Log.e(TAG, "inside getXmlPlayer()");

        String xml = "";

        for( int i = 0 ;  i < userPlayerObj.size() ; i++)
        {
            xml = xml +"<player>" +
                    "<playerId>"+userPlayerObj.get(i).getPlayerid()+"</playerId>"+
                    "<fName>"+userPlayerObj.get(i).getFirstname()+"</fName>"+
                    "<lName>"+userPlayerObj.get(i).getLastname()+"</lName>"+
                    "<grade>"+userPlayerObj.get(i).getGrade()+"</grade>"+
                    "<schoolId>"+userPlayerObj.get(i).getSchoolId()+"</schoolId>"+
                    "<teacherId>"+userPlayerObj.get(i).getTeacherUserId()+"</teacherId>"+
                    "<indexOfAppearance>-1</indexOfAppearance>"+
                    "<profileImageId>"+userPlayerObj.get(i).getImageName()+"</profileImageId>"+
                    "<coins>"+userPlayerObj.get(i).getCoin()+"</coins>"+
                    "<points>"+userPlayerObj.get(i).getPoints()+"</points>"+
                    "<userName>"+userPlayerObj.get(i).getUsername()+"</userName>"+
                    "<competeLevel>1</competeLevel>"+
                    "</player>";
        }

        if(ADD_OR_CREATE_USER_PLAYER_FLAG)
            Log.e(TAG, "outside getXmlPlayer()");

        return xml;
    }



    @Override
    protected void onRestart()
    {
        if(ADD_OR_CREATE_USER_PLAYER_FLAG)
            Log.e(TAG, "inside onRestart()");

        this.setTextFromFacebook();

        if(ADD_OR_CREATE_USER_PLAYER_FLAG)
            Log.e(TAG, "outside onRestart()");

        super.onRestart();
    }

    /**
     * @description Check for valid user from the server
     * @author Yashwant Singh
     *
     */
    class CkeckValidUserAsyncTask extends AsyncTask<Void, Void, Void>
    {
        private String userName = null;
        private String resultValue   = null;

        public CkeckValidUserAsyncTask(String userName)
        {
            this.userName = userName;
        }

        @Override
        protected void onPreExecute()
        {
            progressDialog.show();
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params)
        {
            if(ADD_OR_CREATE_USER_PLAYER_FLAG)
                Log.e(TAG, "inside CkeckValidUserAsyncTask doInBackground()");

            Validation validObj = new Validation();
            resultValue = validObj.validUser(userName);

            if(ADD_OR_CREATE_USER_PLAYER_FLAG)
                Log.e(TAG, "outside CkeckValidUserAsyncTask doInBackground()");

            return null;
        }

        @Override
        protected void onPostExecute(Void result)
        {
            if(ADD_OR_CREATE_USER_PLAYER_FLAG)
                Log.e(TAG, "inside CkeckValidUserAsyncTask onPostExecute()");

            if(resultValue != null){
                if(resultValue.equals("1"))
                {
                    progressDialog.cancel();
                    Translation transeletion = new Translation(AddPlayer.this);
                    transeletion.openConnection();
                    DialogGenerator dg = new DialogGenerator(AddPlayer.this);
                    dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgThisUserNameAlreadyExist"));
                    transeletion.closeConnection();
                }
                else
                {
                    savePlayerData();
                }
            }else{
                CommonUtils.showInternetDialog(AddPlayer.this);
            }
            if(ADD_OR_CREATE_USER_PLAYER_FLAG)
                Log.e(TAG, "outside CkeckValidUserAsyncTask onPostExecute()");

            super.onPostExecute(result);
        }
    }

    /**
     * @description Update Register user on server
     * @author Yashwant Singh
     *
     */
    class UpdateAsynTask extends AsyncTask<Void, Void, Void>
    {
        private RegistereUserDto regObj = null;
        private int registreationResult = 0;

        UpdateAsynTask(RegistereUserDto regObj)
        {
            this.regObj = regObj;
        }

        @Override
        protected void onPreExecute()
        {
            super.onPreExecute();

            if(ADD_OR_CREATE_USER_PLAYER_FLAG)
                Log.e(TAG, "inside UpdateAsynTask onPreExecute()");

            if(ADD_OR_CREATE_USER_PLAYER_FLAG)
                Log.e(TAG, "outside UpdateAsynTask onPreExecute()");
        }

        @Override
        protected Void doInBackground(Void... params)
        {

            if(ADD_OR_CREATE_USER_PLAYER_FLAG)
                Log.e(TAG, "inside UpdateAsynTask doInBackground()");

            Register registerObj = new Register(AddPlayer.this);
            registreationResult = registerObj.updateUserOnserver(regObj);

            if(ADD_OR_CREATE_USER_PLAYER_FLAG)
                Log.e(TAG, "outside UpdateAsynTask doInBackground()");

            return null;
        }

        @Override
        protected void onPostExecute(Void result)
        {
            if(ADD_OR_CREATE_USER_PLAYER_FLAG)
                Log.e(TAG, "inside UpdateAsynTask onPostExecute()");

            progressDialog.cancel();

            Translation transeletion = new Translation(AddPlayer.this);
            transeletion.openConnection();
            DialogGenerator dg = new DialogGenerator(AddPlayer.this);

            if(registreationResult == Register.SUCCESS)
            {
                try{
                    //changes for selected single player (by deepak mail)
                    UserPlayerOperation userObj = new UserPlayerOperation(AddPlayer.this);
                    ArrayList<UserPlayerDto> userPlayerObj = userObj.getUserPlayerData();

                    if(userPlayerObj.size() == 1){
                        SharedPreferences sheredPreference2 = getSharedPreferences(IS_CHECKED_PREFF, 0);
                        SharedPreferences.Editor editor2 = sheredPreference2.edit();
                        editor2.putString(PLAYER_ID,userPlayerObj.get(0).getPlayerid());
                        //changes according to shilpi
                        editor2.putString("userId",userPlayerObj.get(0).getParentUserId());
                        //end changes
                        editor2.commit();
                        AddPlayer.this.startActivity(new Intent(AddPlayer.this,MainActivity.class));
                    }else{
                        if(callingActivity.equals("ModifyRegistration"))
                            AddPlayer.this.startActivity(new Intent(AddPlayer.this,ModifyRegistration.class));
                        else if(callingActivity.equals("LoginUserCreatePlayer"))
                            AddPlayer.this.startActivity(new Intent(AddPlayer.this,LoginUserPlayerActivity.class));
                        else if(callingActivity.equals("LoginUserPlayerActivity"))
                            AddPlayer.this.startActivity(new Intent(AddPlayer.this,LoginUserPlayerActivity.class));
                        else if(callingActivity.equals("CreateTeacherPlayerActivity"))
                            startActivity(new Intent(AddPlayer.this,TeacherPlayer.class));
                        else if(callingActivity.equals("TeacherPlayer"))
                            startActivity(new Intent(AddPlayer.this,TeacherPlayer.class));
                    }
                }catch(Exception e){
                    if(callingActivity.equals("ModifyRegistration"))
                        AddPlayer.this.startActivity(new Intent(AddPlayer.this,ModifyRegistration.class));
                    else if(callingActivity.equals("LoginUserCreatePlayer"))
                        AddPlayer.this.startActivity(new Intent(AddPlayer.this,LoginUserPlayerActivity.class));
                    else if(callingActivity.equals("LoginUserPlayerActivity"))
                        AddPlayer.this.startActivity(new Intent(AddPlayer.this,LoginUserPlayerActivity.class));
                    else if(callingActivity.equals("CreateTeacherPlayerActivity"))
                        startActivity(new Intent(AddPlayer.this,TeacherPlayer.class));
                    else if(callingActivity.equals("TeacherPlayer"))
                        startActivity(new Intent(AddPlayer.this,TeacherPlayer.class));
                }
            }
            else if(registreationResult == Register.INVALID_CITY)
            {
                progressDialog.cancel();
                dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgInvalidCity"));
            }
            else if(registreationResult == Register.INVALID_ZIP)
            {
                progressDialog.cancel();
                dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgInvalidZipCode"));
            }else if(registreationResult == 0){
                CommonUtils.showInternetDialog(AddPlayer.this);
            }

            transeletion.closeConnection();

            if(ADD_OR_CREATE_USER_PLAYER_FLAG)
                Log.e(TAG, "outside UpdateAsynTask onPostExecute()");

            super.onPostExecute(result);
        }
    }
    
    //check only for the last edittext which has current focus
    @Override
    public void onFocusLost(boolean isValidString) {
        onFocusLostIsValidString = isValidString;
    }

    @Override
    public void onFocusHas(boolean isValidString) {

    }
}
