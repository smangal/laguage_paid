package com.mathfriendzy.controller.multifriendzy;

import static com.mathfriendzy.utils.ICommonUtils.FACEBOOK_HOST_NAME;
import static com.mathfriendzy.utils.ICommonUtils.IS_CHECKED_PREFF;
import static com.mathfriendzy.utils.ICommonUtils.MULTIFRIENDZY_ROUND_FLAG;
import static com.mathfriendzy.utils.ICommonUtils.MULTIFRIENDZY_WINNER_SCREEN;
import static com.mathfriendzy.utils.ICommonUtils.PLAYER_ID;
import static com.mathfriendzy.utils.ICommonUtils.PLAYER_INFO;
import static com.mathfriendzy.utils.ITextIds.RATE_URL;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import com.mathfriendzy.controller.base.AdBaseActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore.Images;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.languagefriendzypaid.R;
import com.languagefriendzy.facebookconnect.ShareActivity;
import com.mathfriendzy.controller.multifriendzy.findbyuser.SelectedPlayerActivity;
import com.mathfriendzy.controller.top100.Top100Activity;
import com.mathfriendzy.controller.top100.Top100ListActivity;
import com.mathfriendzy.gcm.ProcessNotification;
import com.mathfriendzy.model.chooseAvtar.ChooseAvtarOpration;
import com.mathfriendzy.model.country.Country;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.learningcenter.CoinsFromServerObj;
import com.mathfriendzy.model.learningcenter.LearningCenteServerOperation;
import com.mathfriendzy.model.learningcenter.LearningCenterimpl;
import com.mathfriendzy.model.multifriendzy.MathFriendzysRoundDTO;
import com.mathfriendzy.model.player.base.Player;
import com.mathfriendzy.model.registration.RegistereUserDto;
import com.mathfriendzy.model.registration.UserPlayerDto;
import com.mathfriendzy.model.registration.UserPlayerOperation;
import com.mathfriendzy.model.registration.UserRegistrationOperation;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.DateTimeOperation;
import com.mathfriendzy.utils.DialogGenerator;

/**
 * This class show the winner congratulation screen for multi frendzy
 * This screen will open when user click on history player of maultifriendzy player
 * and also open when the completion of the multifriendzy
 * @author Yashwant Singh
 *
 */
public class MultiFriendzyWinnerScreen extends AdBaseActivity implements OnClickListener
{
	private TextView labelTop 			= null;
	private TextView txtCongtratulation = null;
	//private TextView txtYouwon 		= null;

	private RelativeLayout imgPlayerLayout 	= null;
	private RelativeLayout imgCupLayout 	= null;
	private RelativeLayout imgOpponentPlayerLayout = null;

	private TextView txtPlayerName 		= null;
	private TextView txtOpponentName 	= null;
	private ImageView imgPlayerImage 	= null;
	private ImageView imgOpponentImage 	= null;
	private ImageView imgCupImage1 		= null;
	private ImageView imgCupImage2 		= null;


	private TextView txtPlayerPoints1 		= null;
	private TextView txtPlayerPoints2 		= null;
	private TextView txtPlayerPoints3		= null;
	private TextView txtPlayerTotalPoints 	= null;

	private TextView txtRound1 				= null;
	private TextView txtRound2 				= null;
	private TextView txtRound3 				= null;
	private TextView txtRoundtotalRound 	= null;

	private TextView txtOpponentPoints1 	= null;
	private TextView txtOpponentPoints2 	= null;
	private TextView txtOpponentPoints3 	= null;
	private TextView txtOpponentTotalPoints = null;

	private RelativeLayout round1layout 	= null;
	private RelativeLayout round2layout 	= null;
	private RelativeLayout round3layout 	= null;
	private RelativeLayout totallayout 		= null;

	private ImageView imgMedel1Round1 = null;
	private ImageView imgMedel2Round1 = null;
	private ImageView imgMedel1Round2 = null;
	private ImageView imgMedel2Round2 = null;
	private ImageView imgMedel1Round3 = null;
	private ImageView imgMedel2Round3 = null;

	private Button btnRematch       = null;
	private Button btnScreenShot 	= null;
	private Button btnMail 			= null;
	private Button btnFbSahre 		= null;
	private Button btnTwitterShare 	= null;

	private String subject					= null;
	private String body						= null;
	private String screenText				= null;

	private Bitmap profileImageBitmap = null;
	private final String TAG = this.getClass().getSimpleName();


	public static String oppenentPlayerName = null;
	public static String opponentImageId 	= null;
	public static boolean isWinner 			= false;

	public static ArrayList<MathFriendzysRoundDTO> roundList = null;	
	private boolean isFromNotifcationForRematch	= false;
	private final int MULTI_FRIENDZY_ITEM_ID = 12;//multi friendzy unlock item id
	

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_multi_friendzy_winner_screen);

		if(MULTIFRIENDZY_WINNER_SCREEN)
			Log.e(TAG, "inside onCreate()");

		if(ProcessNotification.isFromNotification)
		{
			isFromNotifcationForRematch				= true;
			ProcessNotification.isFromNotification 	= false;

			if(ProcessNotification.sentNotificationData.getOppPlayerId().equals(ProcessNotification.sentNotificationData.getWinnerId()))
			{
				MultiFriendzyWinnerScreen.isWinner = true;
			}
			else
			{
				MultiFriendzyWinnerScreen.isWinner = false;
			}

			String opponentNameFromnotificationMsg = "";
			int counter = 1;
			for(int i = 0 ; i < ProcessNotification.sentNotificationData.getMessage().length() ; i ++ )
			{
				if(ProcessNotification.sentNotificationData.getMessage().charAt(i) != ' ' && counter < 2) 
				{
					opponentNameFromnotificationMsg = opponentNameFromnotificationMsg 
							+ ProcessNotification.sentNotificationData.getMessage().charAt(i);
				}
				else
				{
					opponentNameFromnotificationMsg = opponentNameFromnotificationMsg 
							+ ProcessNotification.sentNotificationData.getMessage().charAt(i)
							+ ProcessNotification.sentNotificationData.getMessage().charAt(i + 1);
					counter ++;
				}

				if(counter == 2)
					break;
			}

			oppenentPlayerName = opponentNameFromnotificationMsg;
			opponentImageId    = ProcessNotification.sentNotificationData.getProfileImageId();

			SharedPreferences sharedPrefPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
			SharedPreferences.Editor editor = sharedPrefPlayerInfo.edit();

			UserPlayerOperation userPlayerOpr = new UserPlayerOperation(this);
			UserPlayerDto userPlayerData = userPlayerOpr.getUserPlayerDataById(ProcessNotification.sentNotificationData.getOppPlayerId());
			//UserPlayerDto userPlayerData = userPlayerOpr.getUserPlayerDataById("1221");

			if(userPlayerData != null )
			{
				UserRegistrationOperation userObj = new UserRegistrationOperation(this);
				RegistereUserDto regUserObj = userObj.getUserData();

				editor.clear();
				editor.putString("playerName", userPlayerData.getFirstname() + " " + userPlayerData.getLastname());
				editor.putString("city", regUserObj.getCity());
				editor.putString("state", regUserObj.getState());
				editor.putString("imageName",  userPlayerData.getImageName());
				editor.putString("coins",  userPlayerData.getCoin());
				editor.putInt("grade", Integer.parseInt(userPlayerData.getGrade()));
				editor.putString("userId",  userPlayerData.getParentUserId());
				editor.putString("playerId",  userPlayerData.getPlayerid());
				Country country = new Country();
				editor.putString("countryName", country.getCountryNameByCountryId(regUserObj.getCountryId(), this));
				editor.commit();
			}

			SharedPreferences sheredPreference = getSharedPreferences(IS_CHECKED_PREFF, 0);
			SharedPreferences.Editor editor1 = sheredPreference.edit();
			editor1.clear();
			//editor1.putString(PLAYER_ID,"1221");
			editor1.putString(PLAYER_ID,ProcessNotification.sentNotificationData.getOppPlayerId());
			editor1.commit();
		}

		this.setWidgetsReferences();
		this.setTextFromTranslation();
		this.setPlayerData();
		this.setRoundData();
		this.setListenerOnWidgets();

		if(MULTIFRIENDZY_WINNER_SCREEN)
			Log.e(TAG, "outside onCreate()");

	}

	/**
	 * This method set thet transelaiton text from translation table
	 */
	private void setTextFromTranslation() 
	{

		if(MULTIFRIENDZY_WINNER_SCREEN)
			Log.e(TAG, "inside setTextFromTranslation()");

		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		labelTop.setText(transeletion.getTranselationTextByTextIdentifier("lblBestOf5Friendzy"));
		btnRematch.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleReMatch"));

		txtRound1.setText(transeletion.getTranselationTextByTextIdentifier("lblRound") + " 1");
		txtRound2.setText(transeletion.getTranselationTextByTextIdentifier("lblRound") + " 2");
		txtRound3.setText(transeletion.getTranselationTextByTextIdentifier("lblRound") + " 3");
		txtRoundtotalRound.setText(transeletion.getTranselationTextByTextIdentifier("lblTotal"));

		if(isWinner)
		{
			txtCongtratulation.setText(transeletion.getTranselationTextByTextIdentifier("lblCongratulations")
					+",\n" + transeletion.getTranselationTextByTextIdentifier("lblYouWon") + "!");
			imgCupImage1.setVisibility(ImageView.VISIBLE);
			imgCupImage2.setVisibility(ImageView.GONE);

		}
		else
		{
			txtCongtratulation.setText(transeletion.getTranselationTextByTextIdentifier("lblSorryYouLost"));
			imgCupImage1.setVisibility(ImageView.GONE);
			imgCupImage2.setVisibility(ImageView.VISIBLE);
		}

		screenText 	= transeletion.getTranselationTextByTextIdentifier("alertMsgAScreenShotHasBeenSavedToTheCameraRoll");
		subject		= transeletion.getTranselationTextByTextIdentifier("infoEmailSubject");
		body		= transeletion.getTranselationTextByTextIdentifier("shareFBEmailMessage");

		transeletion.closeConnection();

		if(MULTIFRIENDZY_WINNER_SCREEN)
			Log.e(TAG, "outside setTextFromTranslation()");

	}

	/**
	 * This method set the widgets references
	 */
	private void setWidgetsReferences() 
	{
		if(MULTIFRIENDZY_WINNER_SCREEN)
			Log.e(TAG, "inside setWidgetsReferences()");

		labelTop 			= (TextView) findViewById(R.id.labelTop);
		txtCongtratulation 	= (TextView) findViewById(R.id.txtCongtratulation);
		//txtYouwon 			= (TextView) findViewById(R.id.txtYouwon);

		txtPlayerName 		= (TextView) findViewById(R.id.txtPlayerName);
		txtOpponentName 	= (TextView) findViewById(R.id.txtOpponentName);

		imgPlayerLayout		= (RelativeLayout) findViewById(R.id.imgPlayerLayout);
		//imgCupLayout 		= (RelativeLayout) findViewById(R.id.imgCupLayout);
		imgOpponentPlayerLayout = (RelativeLayout) findViewById(R.id.imgOpponentPlayerLayout);

		imgPlayerImage 		= (ImageView) findViewById(R.id.imgPlayerImage);
		imgOpponentImage	= (ImageView) findViewById(R.id.imgOpponentImage);
		imgCupImage1			= (ImageView) findViewById(R.id.imgCupImage1);
		imgCupImage2            = (ImageView) findViewById(R.id.imgCupImage2);

		txtPlayerPoints1 		= (TextView) findViewById(R.id.txtPlayerPoints1);
		txtPlayerPoints2 		= (TextView) findViewById(R.id.txtPlayerPoints2);
		txtPlayerPoints3 		= (TextView) findViewById(R.id.txtPlayerPoints3);
		txtPlayerTotalPoints 	= (TextView) findViewById(R.id.txtPlayerTotalPoints);

		txtRound1 				= (TextView) findViewById(R.id.txtRound1);
		txtRound2 				= (TextView) findViewById(R.id.txtRound2);
		txtRound3 				= (TextView) findViewById(R.id.txtRound3);
		txtRoundtotalRound 		= (TextView) findViewById(R.id.txtRoundtotalRound);

		txtOpponentPoints1 		= (TextView) findViewById(R.id.txtOpponentPoints1);
		txtOpponentPoints2 		= (TextView) findViewById(R.id.txtOpponentPoints2);
		txtOpponentPoints3 		= (TextView) findViewById(R.id.txtOpponentPoints3);
		txtOpponentTotalPoints 	= (TextView) findViewById(R.id.txtOpponentTotalPoints);

		round1layout 			= (RelativeLayout) findViewById(R.id.round1layout);
		round2layout 			= (RelativeLayout) findViewById(R.id.round2layout);
		round3layout 			= (RelativeLayout) findViewById(R.id.round3layout);
		totallayout				= (RelativeLayout) findViewById(R.id.totalLayout);

		btnRematch            = (Button) findViewById(R.id.btnRematch);
		btnScreenShot         = (Button) findViewById(R.id.btnScreenShot);
		btnMail         	  = (Button) findViewById(R.id.btnMail);
		btnFbSahre            = (Button) findViewById(R.id.btnFbSahre);
		btnTwitterShare       = (Button) findViewById(R.id.btnTwitterShare);

		imgMedel1Round1         = (ImageView) findViewById(R.id.imgMedel1Round1);
		imgMedel2Round1         = (ImageView) findViewById(R.id.imgMedel2Round1);
		imgMedel1Round2         = (ImageView) findViewById(R.id.imgMedel1Round2);
		imgMedel2Round2         = (ImageView) findViewById(R.id.imgMedel2Round2);
		imgMedel1Round3         = (ImageView) findViewById(R.id.imgMedel1Round3);
		imgMedel2Round3         = (ImageView) findViewById(R.id.imgMedel2Round3);

		if(MULTIFRIENDZY_WINNER_SCREEN)
			Log.e(TAG, "outside setWidgetsReferences()");
	}

	/**
	 * This method set the round data
	 */
	private void setRoundData() 
	{	
		if(MULTIFRIENDZY_ROUND_FLAG)
			Log.e(TAG, "inside setRoundData()");

		Translation transeletion = new Translation(this);
		transeletion.openConnection();

		if(roundList.size() > 0)
		{
			if(!(roundList.size() == 3 && 
					(roundList.get(2).getOppScore().length() > 0 
							&& roundList.get(2).getPlayerScore().length() > 0)))
			{
				this.setRoundLayoutBackGround(roundList.size());
			}

			for( int i = 0 ; i < roundList.size() ; i ++ )
			{
				this.setRoundValue(i + 1);
			}

			this.setRoundTotalData();
		}
		else
		{
			this.setRoundLayoutBackGround(1);
			txtPlayerPoints1.setVisibility(TextView.VISIBLE);
			this.setPoints("", txtPlayerPoints1 , transeletion.getTranselationTextByTextIdentifier("lblYourTurn"));
		}

		transeletion.closeConnection();


		//changes
		if(roundList.size() > 0)
		{
			if(roundList.size() < 3)
			{
				if(roundList.get(roundList.size() - 1).getOppScore().length() > 0 
						&& roundList.get(roundList.size() - 1).getPlayerScore().length() > 0)
				{
					this.setRoundLayoutBackGround(roundList.size() + 1);

					if(roundList.size() == 1)
					{
						txtPlayerPoints2.setVisibility(TextView.VISIBLE);
						this.setPoints("", txtPlayerPoints2 ,  transeletion.getTranselationTextByTextIdentifier("lblYourTurn"));
					}
					else
					{
						txtPlayerPoints3.setVisibility(TextView.VISIBLE);
						this.setPoints("", txtPlayerPoints3 ,  transeletion.getTranselationTextByTextIdentifier("lblYourTurn"));
					}
				}
			}
		}
		//end changes

		/*if(roundList.size() < 3)
		{
			if(roundList.get(roundList.size() - 1).getOppScore().length() > 0 
					&& roundList.get(roundList.size() - 1).getPlayerScore().length() > 0)
			{
				this.setRoundLayoutBackGround(roundList.size() + 1);

				if(roundList.size() == 1)
				{
					txtPlayerPoints2.setVisibility(TextView.VISIBLE);
					this.setPoints("", txtPlayerPoints2);
				}
				else
				{
					txtPlayerPoints3.setVisibility(TextView.VISIBLE);
					this.setPoints("", txtPlayerPoints3);
				}
			}
		}
		else
		{
			if(roundList.size() > 0)
			{
				this.setRoundLayoutBackGround(roundList.size());
			}
			else
			{
				this.setRoundLayoutBackGround(1);
			}
		}*/

		for( int i = 0 ; i < roundList.size() ; i ++ )
		{
			if((roundList.get(i).getOppScore().length() > 0 
					&& roundList.get(i).getPlayerScore().length() > 0))
			{
				if((Integer.parseInt(roundList.get(i).getOppScore())) > (Integer.parseInt(roundList.get(i).getPlayerScore())))
				{
					setMedel(i + 1 , false);
				}
				else
				{
					setMedel(i + 1 , true);
				}
			}
		}

		if(MULTIFRIENDZY_ROUND_FLAG)
			Log.e(TAG, "outside setRoundData()");
	}

	/**
	 * This method set the player medel
	 * @param index
	 * @param isPlayerMedel
	 */
	private void setMedel(int index , boolean isPlayerMedel)
	{
		switch(index)
		{
		case 1:
			if(isPlayerMedel)
				imgMedel1Round1.setVisibility(ImageView.VISIBLE);
			else
				imgMedel2Round1.setVisibility(ImageView.VISIBLE);
			break;
		case 2:
			if(isPlayerMedel)
				imgMedel1Round2.setVisibility(ImageView.VISIBLE);
			else
				imgMedel2Round2.setVisibility(ImageView.VISIBLE);
			break;
		case 3:
			if(isPlayerMedel)
				imgMedel1Round3.setVisibility(ImageView.VISIBLE);
			else
				imgMedel2Round3.setVisibility(ImageView.VISIBLE);
			break;
		}
	}

	/**
	 * This method set the round value
	 */
	private void setRoundValue(int index) 
	{
		Translation transeletion = new Translation(this);
		transeletion.openConnection();

		switch(index)
		{
		case 1 : 
			txtPlayerPoints1.setVisibility(TextView.VISIBLE);
			txtOpponentPoints1.setVisibility(TextView.VISIBLE);
			this.setPoints(roundList.get(0).getPlayerScore(), txtPlayerPoints1 , transeletion.getTranselationTextByTextIdentifier("lblYourTurn"));
			this.setPoints(roundList.get(0).getOppScore(), txtOpponentPoints1 , transeletion.getTranselationTextByTextIdentifier("lblTheirTurn"));
			break;

		case 2 : 
			txtPlayerPoints2.setVisibility(TextView.VISIBLE);
			txtOpponentPoints2.setVisibility(TextView.VISIBLE);
			this.setPoints(roundList.get(1).getPlayerScore(), txtPlayerPoints2 , transeletion.getTranselationTextByTextIdentifier("lblYourTurn"));
			this.setPoints(roundList.get(1).getOppScore(), txtOpponentPoints2 , transeletion.getTranselationTextByTextIdentifier("lblTheirTurn"));
			break;

		case 3 : 
			txtPlayerPoints3.setVisibility(TextView.VISIBLE);
			txtOpponentPoints3.setVisibility(TextView.VISIBLE);
			this.setPoints(roundList.get(2).getPlayerScore(), txtPlayerPoints3 , transeletion.getTranselationTextByTextIdentifier("lblYourTurn"));
			this.setPoints(roundList.get(2).getOppScore(), txtOpponentPoints3 , transeletion.getTranselationTextByTextIdentifier("lblTheirTurn"));
			break;
		}

		transeletion.closeConnection();
	}

	/**
	 * This method set the points
	 */
	private void setPoints(String points , TextView txtpoints , String turn)
	{
		if(points.equals(""))
		{
			txtpoints.setText(turn);
		}
		else
		{
			txtpoints.setText(CommonUtils.setNumberString(points));
		}
	}

	/**
	 * This method set the total value of all round
	 */
	private void setRoundTotalData() 
	{
		if(MULTIFRIENDZY_ROUND_FLAG)
			Log.e(TAG, "inside setRoundTotalData()");

		int playerTotal 	= 0 ; 
		int opponentTotal 	= 0 ;

		for(int i = 0 ; i < roundList.size() ; i ++)
		{
			if(!roundList.get(i).getPlayerScore().equals(""))
			{
				playerTotal = playerTotal + Integer.parseInt(roundList.get(i).getPlayerScore());
			}

			if(!roundList.get(i).getOppScore().equals(""))
			{
				opponentTotal = opponentTotal + Integer.parseInt(roundList.get(i).getOppScore());
			}
		}

		txtPlayerTotalPoints.setText(CommonUtils.setNumberString(playerTotal + ""));
		txtOpponentTotalPoints.setText(CommonUtils.setNumberString(opponentTotal + ""));

		if(MULTIFRIENDZY_ROUND_FLAG)
			Log.e(TAG, "outside setRoundTotalData()");
	}

	/**
	 * This metod set the ruond layot background
	 */
	private void setRoundLayoutBackGround(int index)
	{
		switch(index)
		{
		case 1:
			round1layout.setBackgroundResource(R.layout.layout_for_round_1_for_multifriendzy);
			break;
		case 2:
			round2layout.setBackgroundColor(Color.parseColor("#F0E79A"));
			break;
		case 3:
			round3layout.setBackgroundColor(Color.parseColor("#F0E79A"));
			break;
		}
	}

	/**
	 * This method set the player annd oppenent data
	 */
	private void setPlayerData() 
	{
		if(MULTIFRIENDZY_ROUND_FLAG)
			Log.e(TAG, "inside setPlayerData()");

		//set player data
		SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
		String fullName = sharedPreffPlayerInfo.getString("playerName", "");
		String playerName = fullName.substring(0,(fullName.indexOf(" ") + 2));
		//txtPlayerName.setText(sharedPreffPlayerInfo.getString("playerName", ""));
		txtPlayerName.setText(playerName + ".");

		//txtPlayerName.setText(sharedPreffPlayerInfo.getString("playerName", ""));
		String imageName = sharedPreffPlayerInfo.getString("imageName",null);
		this.setImage(imgPlayerImage, imageName);

		//set opponent data
		txtOpponentName.setText(oppenentPlayerName+".");
		this.setImage(imgOpponentImage, opponentImageId);

		if(MULTIFRIENDZY_ROUND_FLAG)
			Log.e(TAG, "outside setPlayerData()");

	}

	/**
	 * This method set the opponent image
	 * @param imgOpponentPlayer
	 * @param imageName
	 */
	private void setImage(ImageView imgPlayer , String imageName)
	{
		try
		{
			Long.parseLong(imageName);
			//changes for Internet Connection
			if(CommonUtils.isInternetConnectionAvailable(this))
			{
				String strUrl = FACEBOOK_HOST_NAME + imageName + "/picture?type=large";
				new FacebookImageLoaderTask(strUrl , imgPlayer).execute(null,null,null);
			}
			else
			{
				imgPlayer.setBackgroundResource(R.drawable.smiley);

			}
		}
		catch(NumberFormatException ee)
		{
			ChooseAvtarOpration chooseAvtarObj = new ChooseAvtarOpration();
			chooseAvtarObj.openConn(this);
			if(chooseAvtarObj.getAvtarImageByName(imageName) != null)
			{
				profileImageBitmap = CommonUtils.getBitmapFromByte(
						chooseAvtarObj.getAvtarImageByName(imageName), this);
				imgPlayer.setImageBitmap(profileImageBitmap);
			}
			chooseAvtarObj.closeConn();
		}
	}


	/**
	 * This method set listener on widgets
	 */
	private void setListenerOnWidgets() 
	{
		btnRematch.setOnClickListener(this);	
		btnMail.setOnClickListener(this);
		btnFbSahre.setOnClickListener(this);
		btnScreenShot.setOnClickListener(this);
		btnTwitterShare.setOnClickListener(this);

	}

	@Override
	public void onClick(View v) 
	{	
		Intent intent = new Intent(this, Top100ListActivity.class);	
		Translation transeletion = null;
		View view = getWindow().getDecorView().getRootView();
		view.setDrawingCacheEnabled(true);
		Bitmap b = view.getDrawingCache();
		Top100Activity.b = b;

		switch(v.getId())
		{
		case R.id.btnRematch : 
			this.clickOnRematch();
			break;
		case R.id.btnScreenShot:
			CommonUtils.saveBitmap(b, "DCIM/Camera", "screen");

			DialogGenerator generator = new DialogGenerator(this);
			transeletion = new Translation(this);
			transeletion.openConnection();
			generator.generateWarningDialog(screenText);
			transeletion.closeConnection();

			break;

		case R.id.btnMail:
			//*******For Sharing ScreenShot ******************//
			String path = Images.Media.insertImage(getContentResolver(), b,"ScreenShot.jpg", null);
			Uri screenshotUri = Uri.parse(path);
			Intent emailIntent = new Intent(     android.content.Intent.ACTION_SEND);

			emailIntent.putExtra(Intent.EXTRA_STREAM, screenshotUri);
			emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, subject);
			emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, body+" "+RATE_URL);
			emailIntent.setType("image/png");
			startActivity(Intent.createChooser(emailIntent, "Send email using"));

			break;

		case R.id.btnFbSahre:		
			intent = new Intent(this, ShareActivity.class);
			intent.putExtra("message", body+" "+RATE_URL);
			//intent.putExtra("bitmap", CommonUtils.scaleDownBitmap(b, 50, this));
			intent.putExtra("flag", false);
			intent.putExtra("id", R.id.btnFbSahre);
			startActivity(intent);
			break;

		case R.id.btnTwitterShare:			
			intent = new Intent(this, ShareActivity.class);
			intent.putExtra("message", body);
			intent.putExtra("flag", false);				
			//intent.putExtra("bitmap", CommonUtils.scaleDownBitmap(b, 50, this));
			intent.putExtra("id", R.id.btnTwitterShare);
			startActivity(intent);
			break;
		}
	}

	/**
	 * This method check for application is unlock or not
	 * @param itemId
	 * @param userId
	 * @return
	 */
	protected int getApplicationUnLockStatus(int itemId,String userId)
	{	
		int appStatus = 0;
		LearningCenterimpl learnignCenterImpl = new LearningCenterimpl(this);
		learnignCenterImpl.openConn();
		appStatus = learnignCenterImpl.getAppUnlockStatus(itemId,userId);
		learnignCenterImpl.closeConn();
		return appStatus;
	}

	/**
	 * This method call when click on rematch button
	 */
	private void clickOnRematch()
	{
		//this.startRound();
		SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
		//Log.e("userId", ""+sharedPreffPlayerInfo.getString("userId", ""));
		if(this.getApplicationUnLockStatus(100 , sharedPreffPlayerInfo.getString("userId", "")) == 1)
		{
			this.startRound();
		}
		else if(this.getApplicationUnLockStatus(MULTI_FRIENDZY_ITEM_ID ,  sharedPreffPlayerInfo.getString("userId", "")) == 1)
		{
			this.startRound();
		}
		else
		{
			String api = "itemId=" + MULTI_FRIENDZY_ITEM_ID + "&userId="+sharedPreffPlayerInfo.getString("userId", "")
					+"&playerId="+sharedPreffPlayerInfo.getString("playerId", "");

			new GetRequiredCoinsForPurchaseItem(api).execute(null,null,null);		
		}
	}

	/**
	 * This method start round and send to the Round Screen
	 */
	private void startRound()
	{
		ArrayList<MathFriendzysRoundDTO> roundList = new ArrayList<MathFriendzysRoundDTO>();

		MultiFriendzyRound.roundList = roundList;

		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		MultiFriendzyRound.turn = transeletion.getTranselationTextByTextIdentifier("lblYourTurn");
		transeletion.closeConnection();

		MultiFriendzyRound.type 		= "your friendzy";
		MultiFriendzyRound.friendzyId 	= "";

		//changes for problem by rematch
		MultiFriendzyRound.opponentImageId     		= opponentImageId;
		MultiFriendzyRound.oppenentPlayerName    	= oppenentPlayerName;
		MultiFriendzyMain.isNewFriendzyStart		= true;
		setSelectedPlayerData();
		//end changes
		startActivity(new Intent(this , MultiFriendzyRound.class));
	}


	//changes for rematch
	private void setSelectedPlayerData(){

		Player user = new Player();
		  if(isFromNotifcationForRematch){
		   user.setFirstName(oppenentPlayerName);
		      user.setLastName(" ");
		      user.setPlayerId(Integer.parseInt(ProcessNotification.sentNotificationData.getPlayerId()));
		      user.setProfileImageName(opponentImageId);
		      user.setParentUserId(Integer.parseInt(ProcessNotification.sentNotificationData.getUserId()));
		      user.setAndroidPids((ProcessNotification.sentNotificationData.getSenderDeviceId()));//this may be 
		                          //android or iphone
		      //user.setIponPids(MultiFriendzyRound.multiFriendzyServerDto.getInitiatorId());
		  }else{
		   user.setFirstName(MultiFriendzyRound.multiFriendzyServerDto.getOpponentData().getfName());
		      user.setLastName(MultiFriendzyRound.multiFriendzyServerDto.getOpponentData().getlName());
		      user.setPlayerId(Integer.parseInt(MultiFriendzyRound.multiFriendzyServerDto.getOpponentData().getPlayerId()));
		      user.setProfileImageName(MultiFriendzyRound.multiFriendzyServerDto.getOpponentData().getProfileImageNameId());
		      user.setParentUserId(Integer.parseInt(MultiFriendzyRound.multiFriendzyServerDto.getOpponentData().getParentUserId()));
		      user.setAndroidPids(MultiFriendzyRound.multiFriendzyServerDto.getAndroidPids());
		      user.setIponPids(MultiFriendzyRound.multiFriendzyServerDto.getInitiatorId());
		  }
		SelectedPlayerActivity.player = user;		
	}


	/*
	 *//**
	 * use to check user exist or not if exist then go for its detail
	 * @author Shilpi Mangal
	 *
	 *//*
	class AsyncUserFormServer extends AsyncTask<Void, Void, Void>
	{
		Context context;
		ProgressDialog pd;
		String strUrl;

		public AsyncUserFormServer(Context context, String username) 
		{			
			this.context = context;
			pd = CommonUtils.getProgressDialog(context);
			pd.show();
			user = new Player();

			strUrl = ICommonUtils.COMPLETE_URL + "action=getPlayersByUserNameAndroid&uname="+username +
					"&userId="+userId+"&playerId="+playerId+"&appId=" + CommonUtils.APP_ID;

			String action = "getPlayersByUserNameAndroid";
			strUrl = ICommonUtils.CONPLETE_URL_FOR_NOTIFICATION + "action=" + action + "&"
					 + "uname=" + username + "&"
					 + "userId="+ userId   + "&"
					 + "playerId=" + playerId + "&"
					 + "appId=" + CommonUtils.APP_ID;
		}

		@Override
		protected Void doInBackground(Void... arg0)
		{
			//Log.e("SelectedPlayerActivity", "url : " + strUrl);
			String jsonStr = CommonUtils.readFromURL(strUrl);
			//Log.e("SelectedPlayerActivity", jsonStr);
			user = JsonFileParser.getUserDetail(jsonStr, context);			
			return null;
		}


		@Override
		protected void onPostExecute(Void result) 
		{	

			super.onPostExecute(result);
		}
	}*/



	@Override
	public void onBackPressed() 
	{
		startActivity(new Intent(this , MultiFriendzyMain.class));

		super.onBackPressed();
	}


	/**
	 * This asyncTask set image from facebook url to the Button 
	 * @author Yashwant Singh
	 *
	 */
	class FacebookImageLoaderTask extends AsyncTask<Void, Void, Void>
	{
		private String strUrl = null;
		private ImageView imgPlayer = null;
		public FacebookImageLoaderTask(String strUrl , ImageView imgPlayer)
		{
			this.strUrl = strUrl;
			this.imgPlayer = imgPlayer;
		}

		@Override
		protected Void doInBackground(Void... params) 
		{				
			URL img_value;
			try 
			{
				img_value = new URL(strUrl);
				profileImageBitmap = BitmapFactory.decodeStream(img_value.openConnection().getInputStream());
			} 
			catch (MalformedURLException e) 
			{			
				e.printStackTrace();
				Log.e("EditActivity", "Problem in setting image" + e);
			}
			catch(Exception ee)
			{
				Log.e("EditActivity", "Problem in setting image" + ee);
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) 
		{
			imgPlayer.setImageBitmap(profileImageBitmap);
			imgPlayer.invalidate();
			super.onPostExecute(result);
		}
	}

	/**
	 * This class get coins from server
	 * @author Yashwant Singh
	 *
	 */
	class GetRequiredCoinsForPurchaseItem extends AsyncTask<Void, Void, Void>
	{
		private String apiString = null;
		private CoinsFromServerObj coindFromServer;
		private ProgressDialog pg = null;

		GetRequiredCoinsForPurchaseItem(String apiValue)
		{
			this.apiString = apiValue;
		}

		@Override
		protected void onPreExecute() 
		{
			pg = CommonUtils.getProgressDialog(MultiFriendzyWinnerScreen.this);
			pg.show();
			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) 
		{
			LearningCenteServerOperation learnignCenterOpr = new LearningCenteServerOperation();
			coindFromServer = learnignCenterOpr.getRequiredCoisForPurchase(apiString);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) 
		{			
			pg.cancel();

			/*if(coindFromServer.getCoinsEarned() == -1 || coindFromServer.getCoinsEarned() >= coindFromServer.getCoinsRequired() 
					|| coindFromServer.getCoinsPurchase() == 0)
			{
				*/if(coindFromServer.getCoinsEarned() == -1)
				{
					LearningCenterimpl learnignCenterObj = new LearningCenterimpl(MultiFriendzyWinnerScreen.this);
					learnignCenterObj.openConn();
					DialogGenerator dg = new DialogGenerator(MultiFriendzyWinnerScreen.this);
					dg.generateLevelWarningDialogForLearnignCenter(learnignCenterObj.getDataFromPlayerTotalPoints("0").getCoins() ,
							coindFromServer , MULTI_FRIENDZY_ITEM_ID, null);
					learnignCenterObj.closeConn();
				}
				else
				{	
					DateTimeOperation numberformat = new DateTimeOperation();
					Translation transeletion = new Translation(MultiFriendzyWinnerScreen.this);
					transeletion.openConnection();
					String msg = transeletion.getTranselationTextByTextIdentifier("alertMsgFriendzyPurchaseYouMustUpgrade") + " " +
							numberformat.setNumberString(coindFromServer.getCoinsRequired() + "") + " " + transeletion.getTranselationTextByTextIdentifier("lblCoins");
					transeletion.closeConnection();

					DialogGenerator dg = new DialogGenerator(MultiFriendzyWinnerScreen.this);
					dg.generateLevelWarningDialogForLearnignCenter(coindFromServer.getCoinsEarned() ,
							coindFromServer , MULTI_FRIENDZY_ITEM_ID , msg);
				}
			/*}
			else
			{
				DialogGenerator dg = new DialogGenerator(MultiFriendzyWinnerScreen.this);
				dg.generateDialogForNeedCoinsForUlock(coindFromServer.getCoinsEarned() ,
						coindFromServer.getCoinsRequired());
			}*/
			super.onPostExecute(result);
		}
	}
}

