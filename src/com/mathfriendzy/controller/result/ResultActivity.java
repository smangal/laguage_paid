package com.mathfriendzy.controller.result;

import static com.mathfriendzy.utils.ITextIds.LBL_GEOGRAPHY;
import static com.mathfriendzy.utils.ITextIds.LBL_LANGUAGE;
import static com.mathfriendzy.utils.ITextIds.LBL_LIFETIME;
import static com.mathfriendzy.utils.ITextIds.LBL_MATH;
import static com.mathfriendzy.utils.ITextIds.LBL_MEASUREMENT;
import static com.mathfriendzy.utils.ITextIds.LBL_MONEY;
import static com.mathfriendzy.utils.ITextIds.LBL_PHONICS;
import static com.mathfriendzy.utils.ITextIds.LBL_POINTS;
import static com.mathfriendzy.utils.ITextIds.LBL_READING;
import static com.mathfriendzy.utils.ITextIds.LBL_SCIENCE;
import static com.mathfriendzy.utils.ITextIds.LBL_SCORE;
import static com.mathfriendzy.utils.ITextIds.LBL_SELECTDATE;
import static com.mathfriendzy.utils.ITextIds.LBL_SOCIAL;
import static com.mathfriendzy.utils.ITextIds.LBL_TIME;
import static com.mathfriendzy.utils.ITextIds.LBL_VOCABULARY;
import static com.mathfriendzy.utils.ITextIds.MF_HOMESCREEN;
import static com.mathfriendzy.utils.ITextIds.MF_RESULT;
import static com.mathfriendzy.utils.ITextIds.RATE_URL;

import java.util.ArrayList;

import com.mathfriendzy.controller.base.AdBaseActivity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore.Images;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.languagefriendzypaid.R;
import com.languagefriendzy.facebookconnect.ShareActivity;
import com.mathfriendzy.controller.top100.Top100Activity;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.result.JsonAsynTaskForDate;
import com.mathfriendzy.model.top100.JsonFileParser;
import com.mathfriendzy.model.top100.Score;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.DateTimeOperation;
import com.mathfriendzy.utils.DialogGenerator;


public class ResultActivity extends AdBaseActivity implements OnClickListener
{
	private TextView txtTitle			= null;
	private TextView txtPlayerName		= null;
	private TextView lifetTime			= null;
	private TextView time				= null;
	private TextView txtTime			= null;
	private TextView points				= null;
	private TextView txtPoints			= null;
	private TextView txtMathFriendzy	= null;
	private TextView txtTimeList		= null;
	private TextView txtScore			= null;
	private TextView txtDate			= null;

	private TextView txtMathTotal		= null;
	private TextView txtTotalTime		= null;
	private TextView txtAvgScore		= null;

	private TextView[] txtOperation		= null;
	private TextView[] txtTimes			= null;
	private TextView[] txtScores		= null;

	private Button	 btnDate			= null;
	private RelativeLayout layoutList	= null;

	private String playerName			= null;
	private int userId					= 0;
	private int playerId				= 0;
	private String playDate				= null;
	private JsonFileParser file			= null;
	private ArrayList<Score> scoreList	= null;
	private String jsonFile;

	private boolean SELECT_FLAG			= false;


	private Button btnShare					= null;
	private Button btnCloseShareTool		= null;
	private LinearLayout layoutShare		= null;
	private Button btnScreenShot			= null;
	private Button btnEmail					= null;
	private Button btnTwitter				= null;
	private Button btnFbShare				= null;
	private Bitmap b;
	private String subject					= null;
	private String body						= null;
	private String screenText				= null;

	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_result);

		playerName  = getIntent().getStringExtra("playerName");
		userId		= getIntent().getIntExtra("userId", 0);
		playerId	= getIntent().getIntExtra("playerId", 0);		
		playDate 	= getIntent().getStringExtra("date");
		SELECT_FLAG	= getIntent().getBooleanExtra("flag", false);

		file = new JsonFileParser(this);
		jsonFile = getIntent().getStringExtra("jsonFile");
		scoreList		= new ArrayList<Score>();	
		//Log.e("", "jsonFile " +jsonFile);
		scoreList 		= file.getMathScore(jsonFile);

		LayoutInflater inflater = (LayoutInflater) this.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		RelativeLayout b = (RelativeLayout)inflater.inflate(R.layout.math_result_list, null);
		getWidgetId();		
		layoutList.addView(b);

		setWigdetIdToArray();
		setWidgetText();
	}//END onClick method



	private void setWigdetIdToArray()
	{
		txtOperation = new TextView[10];
		txtTimes	 = new TextView[10];
		txtScores	 = new TextView[10];

		txtOperation[0] = (TextView) findViewById(R.id.txtAddition);
		txtOperation[1] = (TextView) findViewById(R.id.txtSubtraction);
		txtOperation[2] = (TextView) findViewById(R.id.txtMultiplication);
		txtOperation[3] = (TextView) findViewById(R.id.txtDivision);
		txtOperation[4] = (TextView) findViewById(R.id.txtAddFraction);
		txtOperation[5] = (TextView) findViewById(R.id.txtDivFraction);
		txtOperation[6] = (TextView) findViewById(R.id.txtAddDecimal);
		txtOperation[7] = (TextView) findViewById(R.id.txtDivDecimal);
		txtOperation[8] = (TextView) findViewById(R.id.txtAddNaegative);
		txtOperation[9] = (TextView) findViewById(R.id.txtDivNegatives);

		txtTimes[0]		= (TextView) findViewById(R.id.txtTime1);
		txtTimes[1]		= (TextView) findViewById(R.id.txtTime2);
		txtTimes[2]		= (TextView) findViewById(R.id.txtTime3);
		txtTimes[3]		= (TextView) findViewById(R.id.txtTime4);
		txtTimes[4]		= (TextView) findViewById(R.id.txtTime5);
		txtTimes[5]		= (TextView) findViewById(R.id.txtTime6);
		txtTimes[6]		= (TextView) findViewById(R.id.txtTime7);
		txtTimes[7]		= (TextView) findViewById(R.id.txtTime8);
		txtTimes[8]		= (TextView) findViewById(R.id.txtTime9);
		txtTimes[9]		= (TextView) findViewById(R.id.txtTime10);

		txtScores[0]	= (TextView) findViewById(R.id.txtScore1);
		txtScores[1]	= (TextView) findViewById(R.id.txtScore2);
		txtScores[2]	= (TextView) findViewById(R.id.txtScore3);
		txtScores[3]	= (TextView) findViewById(R.id.txtScore4);
		txtScores[4]	= (TextView) findViewById(R.id.txtScore5);
		txtScores[5]	= (TextView) findViewById(R.id.txtScore6);
		txtScores[6]	= (TextView) findViewById(R.id.txtScore7);
		txtScores[7]	= (TextView) findViewById(R.id.txtScore8);
		txtScores[8]	= (TextView) findViewById(R.id.txtScore9);
		txtScores[9]	= (TextView) findViewById(R.id.txtScore10);

		txtMathTotal	= (TextView) findViewById(R.id.txtMathTotal);
		txtTotalTime	= (TextView) findViewById(R.id.txtTotalTime);
		txtAvgScore		= (TextView) findViewById(R.id.txtAvgScore);
	}



	private void getWidgetId()
	{
		txtTime			= (TextView) findViewById(R.id.txtTime);
		txtDate			= (TextView) findViewById(R.id.txtDate);
		txtMathFriendzy = (TextView) findViewById(R.id.txtMathFriendzy);
		txtPlayerName 	= (TextView) findViewById(R.id.txtPlayerName);
		txtPoints		= (TextView) findViewById(R.id.txtPoints);
		txtScore 		= (TextView) findViewById(R.id.txtScore);
		txtTimeList 	= (TextView) findViewById(R.id.txtTimeList);
		txtTitle 		= (TextView) findViewById(R.id.txtTitleScreen);
		time			= (TextView) findViewById(R.id.time);
		points			= (TextView) findViewById(R.id.points);
		btnDate			= (Button) findViewById(R.id.btnDate);
		lifetTime		= (TextView) findViewById(R.id.lifeTime);
		layoutList		= (RelativeLayout) findViewById(R.id.lay);

		layoutShare			= (LinearLayout) findViewById(R.id.layoutShare);
		btnShare			= (Button) findViewById(R.id.btnShare);
		btnEmail			= (Button) findViewById(R.id.btnMail);
		btnFbShare			= (Button) findViewById(R.id.btnFbSahre);
		btnScreenShot		= (Button) findViewById(R.id.btnScreenShot);
		btnTwitter			= (Button) findViewById(R.id.btnTwitterShare);
		btnCloseShareTool	= (Button) findViewById(R.id.btnCloseShareToolbar);

		btnDate.setOnClickListener(this);		
		btnCloseShareTool.setOnClickListener(this);
		btnShare.setOnClickListener(this);
		btnEmail.setOnClickListener(this);
		btnFbShare.setOnClickListener(this);
		btnScreenShot.setOnClickListener(this);
		btnTwitter.setOnClickListener(this);
	}	


	private Bitmap getSceenShot(){
		View view = getWindow().getDecorView().getRootView();
		view.setDrawingCacheEnabled(true);
		Bitmap b = view.getDrawingCache();
		return b;
	}

	@Override
	public void onClick(View v) 
	{		
		layoutShare.setVisibility(View.GONE);
		btnCloseShareTool.setVisibility(View.GONE);
		b = getSceenShot();
		Top100Activity.b = b;
		Intent intent;
		
		switch(v.getId()){
		case R.id.btnDate:
			if(!SELECT_FLAG){			
				new JsonAsynTaskForDate(this, userId, playerId,playerName,0)
				.execute(null,null,null);			
			}
			else{
				onBackPressed();
			}
			break;
		case R.id.btnShare:
			layoutShare.setVisibility(View.VISIBLE);
			btnCloseShareTool.setVisibility(View.VISIBLE);			
			break;

		case R.id.btnCloseShareToolbar:
			break;

		case R.id.btnScreenShot:
			CommonUtils.saveBitmap(b, "DCIM/Camera", "screen");

			DialogGenerator generator = new DialogGenerator(this);
			Translation transeletion = new Translation(this);
			transeletion.openConnection();
			generator.generateWarningDialog(screenText);
			transeletion.closeConnection();

			break;

		case R.id.btnMail:
			//*******For Sharing ScreenShot ******************//
			String path = Images.Media.insertImage(getContentResolver(), b,"ScreenShot.jpg", null);
			Uri screenshotUri = Uri.parse(path);
			Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);

			emailIntent.putExtra(Intent.EXTRA_STREAM, screenshotUri);
			emailIntent.putExtra(android.content.Intent.EXTRA_SUBJECT, subject);
			emailIntent.putExtra(android.content.Intent.EXTRA_TEXT, body+" "+RATE_URL);
			emailIntent.setType("image/png");
			try 
			{
				startActivity(Intent.createChooser(emailIntent, "Send email using"));
			} catch (android.content.ActivityNotFoundException ex) {
				Toast.makeText(this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
			}

			break;

		case R.id.btnFbSahre:				
			intent = new Intent(this, ShareActivity.class);
			intent.putExtra("message", body+" "+RATE_URL);
			//intent.putExtra("bitmap", CommonUtils.scaleDownBitmap(b, 50, this));
			intent.putExtra("flag", false);
			intent.putExtra("id", R.id.btnFbSahre);
			startActivity(intent);

			break;

		case R.id.btnTwitterShare:			
			intent = new Intent(this, ShareActivity.class);
			intent.putExtra("message", body);
			intent.putExtra("flag", false);
			//intent.putExtra("bitmap", CommonUtils.scaleDownBitmap(b, 50, this));
			intent.putExtra("id", R.id.btnTwitterShare);
			startActivity(intent);
			break;
		}//End switch case
	}	
	

	private void setWidgetText() 
	{
		DateTimeOperation date = new DateTimeOperation();
		String text;
		int totalTime = 0, totalScore = 0;

		Translation translate = new Translation(this);
		translate.openConnection();

		screenText 	= translate.getTranselationTextByTextIdentifier("alertMsgAScreenShotHasBeenSavedToTheCameraRoll");
		subject		= translate.getTranselationTextByTextIdentifier("infoEmailSubject");
		body		= translate.getTranselationTextByTextIdentifier("shareFBEmailMessage");
		
		text = translate.getTranselationTextByTextIdentifier(MF_HOMESCREEN);
		txtMathFriendzy.setText(text+":");
		txtMathTotal.setText(text+" " +translate.getTranselationTextByTextIdentifier("lblTotal"));

		text = translate.getTranselationTextByTextIdentifier(MF_RESULT);
		txtTitle.setText(text);

		text = translate.getTranselationTextByTextIdentifier(LBL_LIFETIME);
		lifetTime.setText(text+":");

		text = translate.getTranselationTextByTextIdentifier(LBL_POINTS);
		points.setText(text+":");

		text = translate.getTranselationTextByTextIdentifier(LBL_SCORE);
		txtScore.setText(text+":");

		text = translate.getTranselationTextByTextIdentifier(LBL_TIME);
		time.setText(text+":");
		txtTimeList.setText(text+":");

		text = translate.getTranselationTextByTextIdentifier(LBL_SELECTDATE);
		btnDate.setText(text);

		text = translate.getTranselationTextByTextIdentifier(LBL_READING);
		txtOperation[0].setText(text);
		text = translate.getTranselationTextByTextIdentifier(LBL_MATH);
		txtOperation[1].setText(text);
		text = translate.getTranselationTextByTextIdentifier(LBL_LANGUAGE);
		txtOperation[2].setText(text);
		text = translate.getTranselationTextByTextIdentifier(LBL_VOCABULARY);
		txtOperation[3].setText(text);

		text = translate.getTranselationTextByTextIdentifier(LBL_MONEY);
		txtOperation[4].setText(text);
		text = translate.getTranselationTextByTextIdentifier(LBL_PHONICS);
		txtOperation[5].setText(text);

		text = translate.getTranselationTextByTextIdentifier(LBL_MEASUREMENT);
		txtOperation[6].setText(text);
		text = translate.getTranselationTextByTextIdentifier(LBL_GEOGRAPHY);
		txtOperation[7].setText(text);

		text = translate.getTranselationTextByTextIdentifier(LBL_SOCIAL);
		txtOperation[8].setText(text);
		text = translate.getTranselationTextByTextIdentifier(LBL_SCIENCE);
		txtOperation[9].setText(text);

		translate.closeConnection();

		txtPlayerName.setText(playerName);
		txtTime.setText(date.setTimeFormat(file.getTotalTime(jsonFile)));
		txtPoints.setText(date.setNumberString(file.getTotalPoints(jsonFile)));

		for(int i = 0; i < scoreList.size(); i++)
		{
			Score obj = new Score();
			obj = scoreList.get(i);
			int id = Integer.parseInt(obj.getId()) - 1;

			totalScore = totalScore + (Integer.parseInt(obj.getScore()));
			totalTime  = totalTime + Integer.parseInt(obj.getTime());

			txtTimes[id].setText(date.setTimeFormat(Integer.parseInt(obj.getTime())));
			txtScores[id].setText(obj.getScore()+"%");
		}
		txtTotalTime.setText(""+date.setTimeFormat(totalTime));
		if(scoreList.size() > 0)
		{
			txtAvgScore.setText(""+totalScore/scoreList.size()+"%");
		}
		txtDate.setText(date.setDate(playDate));
	}

	
	@Override
	public void onBackPressed() 
	{		
		super.onBackPressed();
	}
	
}
