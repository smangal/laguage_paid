package com.mathfriendzy.controller.friendzy;

import java.util.ArrayList;

import com.mathfriendzy.controller.base.AdBaseActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.languagefriendzypaid.R;
import com.mathfriendzy.controller.main.MainActivity;
import com.mathfriendzy.customview.DynamicLayout;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.registration.UserPlayerDto;
import com.mathfriendzy.model.registration.UserPlayerOperation;

public class FriendzyNotificationPlayerActivity extends AdBaseActivity
{
	private TextView mfTitleHomeScreen 				= null;
	private TextView txtTitlePlayers 				= null;

	private ArrayList<UserPlayerDto> userPlayerList = null;
	private LinearLayout linearLayout 				= null;
	private String strViewChallenge					= null;


	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_friendzy_notification_player);

		getUserPlayersData();
		setWidgetsReferences();
		setWidgetsTextValue();
		
		DynamicLayout dynamicLayout = new DynamicLayout(this);
		dynamicLayout.createDynamicLayoutForFriendzy(userPlayerList, linearLayout,strViewChallenge);
	}

	/**
	 * This method set the wodgets refernces from the layout to the widgets objects
	 */
	private void setWidgetsReferences()
	{		
		mfTitleHomeScreen 		= (TextView) findViewById(R.id.mfTitleHomeScreen);
		txtTitlePlayers		 	= (TextView) findViewById(R.id.txtTitlePlayers);
		linearLayout            = (LinearLayout) findViewById(R.id.userPlayerLayout);

	}

	/**
	 * this method set the widgets text values from the translation
	 */
	private void setWidgetsTextValue()
	{
		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		mfTitleHomeScreen.setText(transeletion.getTranselationTextByTextIdentifier("homeTitleFriendzy"));
		txtTitlePlayers.setText(transeletion.getTranselationTextByTextIdentifier("lblPleaseSelectPlayer"));
		strViewChallenge = transeletion.getTranselationTextByTextIdentifier("lblViewChallenges");
		
		transeletion.closeConnection();
	}


	/**
	 * this method get user player data from th database
	 */
	private void getUserPlayersData() 
	{		
		UserPlayerOperation userPlayerObj = new UserPlayerOperation(this);
		if(userPlayerObj.isUserPlayerTableExist())
		{
			userPlayerList = userPlayerObj.getUserPlayerData();
		}
		
	}


	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) 
	{
		if(keyCode == KeyEvent.KEYCODE_BACK)
		{

			Intent intentMain = new Intent(this,MainActivity.class);
			startActivity(intentMain);
			finish();

			return false;
		}
		return super.onKeyDown(keyCode, event);
	}

}
