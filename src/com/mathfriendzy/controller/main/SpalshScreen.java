package com.mathfriendzy.controller.main;

import static com.mathfriendzy.utils.ICommonUtils.DEVICE_ID;
import static com.mathfriendzy.utils.ICommonUtils.DEVICE_ID_PREFF;
import static com.mathfriendzy.utils.ICommonUtils.SPLASH_ACTIVITY_LOG;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.util.Log;

import com.languagefriendzypaid.R;
import com.mathfriendzy.gcm.GCMRegistration;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.DatabaseAdapter;
import com.mathfriendzy.utils.DialogGenerator;
import com.mathfriendzy.utils.StartUpAsyncTask;

public class SpalshScreen extends Activity 
{

	//public static StartUpAsyncTask asynCkTaskObj = null;
	public static boolean CLEAR_FLAG_SHARED_PREFERENCES	= false;
	private static boolean isGetLanguageFromServer 	= false;
	private static boolean isTranselation 			= false;
	public static String newversion					= "1.0";
	public static String oldversion					= "0.0";

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);

		if(SPLASH_ACTIVITY_LOG)
			Log.e("SplashActivity", "inside onCreate()");

		//Use to Save Database file into database folder
		DatabaseAdapter adapter = new DatabaseAdapter(this);

		//set the device id in shared preffrences
		SharedPreferences sharedPreff = getSharedPreferences(DEVICE_ID_PREFF, 0);		
		SharedPreferences.Editor editor = sharedPreff.edit();
		editor.putString(DEVICE_ID, MathFriendzyHelper.getDeviceId(this));
		editor.commit();

		PackageInfo pInfo;
		try {
			pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
			newversion = pInfo.versionName;
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}

		SharedPreferences sheredPreference = getSharedPreferences("SHARED_FLAG", 0);					
		oldversion				= sheredPreference.getString("oldversion", "1.8");

		//Use to check new version of apk
		//newversion = "1.1";
		if(!oldversion.equals(newversion))
		{			
			MathFriendzyHelper.saveUserLogin(this , false);
			adapter.deleteDataBaseIfExist();
			adapter.createDatabase();
			CommonUtils.setSharedPreferences(this);
		}

		isGetLanguageFromServer = sheredPreference.getBoolean("isGetLanguageFromServer", false);
		isTranselation			= sheredPreference.getBoolean("isTranselation", false);

		if(CommonUtils.isInternetConnectionAvailable(this))	{
			new GCMRegistration(this , false);
			new StartUpAsyncTask(this).execute(null,null,null);
		}else if(isGetLanguageFromServer && isTranselation){
			Intent intent = new Intent(this , MainActivity.class);
			startActivity(intent);
			finish();
		}else{			
			DialogGenerator dg = new DialogGenerator(this);
			dg.generateInternetWarningDialog("You are not connected to the internet.\nPlease try again later.", "ok");
			//finish();
		}
		if(SPLASH_ACTIVITY_LOG)
			Log.e("SplashActivity", "outside onCreate()");
	}
}
