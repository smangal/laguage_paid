package com.mathfriendzy.model.multifriendzy;

public class OppnentDataDTO 
{
	private String fName;
	private String lName;
	private String schoolId ;
	private String schoolName;
	private String grade;
	private String teacherUserid;
	private String teacherFirstName;
	private String teacherLastName;
	private String indexOfAppearance;
	private String parentUserId;
	private String playerId;
	private String completeLevel;
	private String profileImageNameId;
	private String coins;
	private String points;
	private String city;
	private String state;
	private String userName;
	
	public String getfName() {
		return fName;
	}
	public void setfName(String fName) {
		this.fName = fName;
	}
	public String getlName() {
		return lName;
	}
	public void setlName(String lName) {
		this.lName = lName;
	}
	public String getSchoolId() {
		return schoolId;
	}
	public void setSchoolId(String schoolId) {
		this.schoolId = schoolId;
	}
	public String getSchoolName() {
		return schoolName;
	}
	public void setSchoolName(String schoolName) {
		this.schoolName = schoolName;
	}
	public String getGrade() {
		return grade;
	}
	public void setGrade(String grade) {
		this.grade = grade;
	}
	public String getTeacherUserid() {
		return teacherUserid;
	}
	public void setTeacherUserid(String teacherUserid) {
		this.teacherUserid = teacherUserid;
	}
	public String getTeacherFirstName() {
		return teacherFirstName;
	}
	public void setTeacherFirstName(String teacherFirstName) {
		this.teacherFirstName = teacherFirstName;
	}
	public String getTeacherLastName() {
		return teacherLastName;
	}
	public void setTeacherLastName(String teacherLastName) {
		this.teacherLastName = teacherLastName;
	}
	public String getIndexOfAppearance() {
		return indexOfAppearance;
	}
	public void setIndexOfAppearance(String indexOfAppearance) {
		this.indexOfAppearance = indexOfAppearance;
	}
	public String getParentUserId() {
		return parentUserId;
	}
	public void setParentUserId(String parentUserId) {
		this.parentUserId = parentUserId;
	}
	public String getPlayerId() {
		return playerId;
	}
	public void setPlayerId(String playerId) {
		this.playerId = playerId;
	}
	public String getCompleteLevel() {
		return completeLevel;
	}
	public void setCompleteLevel(String completeLevel) {
		this.completeLevel = completeLevel;
	}
	public String getProfileImageNameId() {
		return profileImageNameId;
	}
	public void setProfileImageNameId(String profileImageNameId) {
		this.profileImageNameId = profileImageNameId;
	}
	public String getCoins() {
		return coins;
	}
	public void setCoins(String coins) {
		this.coins = coins;
	}
	public String getPoints() {
		return points;
	}
	public void setPoints(String points) {
		this.points = points;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getUserName() {
		return userName;
	}
	public void setUserName(String userName) {
		this.userName = userName;
	}
	
	
	
}
