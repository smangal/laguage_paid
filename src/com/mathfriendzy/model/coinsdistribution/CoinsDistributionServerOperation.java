package com.mathfriendzy.model.coinsdistribution;

import static com.mathfriendzy.utils.ICommonUtils.COMPLETE_URL;

import com.mathfriendzy.utils.CommonUtils;

/**
 * This class for server operation for coins
 * @author Yashwant Singh
 *
 */
public class CoinsDistributionServerOperation 
{
	/**
	 * This method update coins on server
	 * @param userId
	 * @param playerId
	 * @param userCoins
	 * @param playerCoins
	 */
	public void updateCoinsOnServerForUserAndPlayer(String userId , String playerId , int userCoins , int playerCoins)
	{
		String action = "saveCoinsForPlayerAndUser";
		
		String strUrl = COMPLETE_URL  + "action=" + action + "&"
				+ "user_id="   + userId  + "&"
				+ "userCoins=" + userCoins + "&"
				+ "player_id=" + playerId + "&"
				+ "playerCoins=" + playerCoins+"&appId="+CommonUtils.APP_ID;
		
		//Log.e("CoinsDistributionServerOperation", "inside updateCoinsOnServerForUserAndPlayer url : " + strUrl);
		
		this.parseUpdateCoinsJson(CommonUtils.readFromURL(strUrl));
	}
	
	private void parseUpdateCoinsJson(String jsonString)
	{
		//Log.e("CoinsDistributionServerOperation", "inside parseUpdateCoinsJson json String " + jsonString);
	}
	
	/**
	 * This method save coins and item status on server
	 * @param itemId
	 * @param userId
	 * @param userCoins
	 * @param playerId
	 * @param playerCoins
	 */
	public void saveCoinsAndItemStatus(int itemId , String userId , int userCoins , String playerId , int playerCoins)
	{
		String action = "saveCoinsAndItemStatus";
		
		String strUrl = COMPLETE_URL  + "action=" + action + "&"
				+ "itemId="   + itemId  + "&"
				+ "userId=" + userId + "&"
				+ "userCoins=" + userCoins + "&"
				+ "playerId=" + playerId + "&"
				+ "playerCoins=" + playerCoins+"&appId="+CommonUtils.APP_ID;
		
		//Log.e("CoinsDistributionServerOperation", "inside saveCoinsAndItemStatus url : " + strUrl);
		this.parseUpdateCoinsJson(CommonUtils.readFromURL(strUrl));
	}
}
